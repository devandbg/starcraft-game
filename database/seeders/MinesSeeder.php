<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MinesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mines')->insert([
            'id' => 1,
            'name' => 'Mineral Mine',
            'price' => 0
        ]);

        DB::table('mines')->insert([
            'id' => 2,
            'name' => 'Gas Mine',
            'price' => 750,
        ]);
    }
}
