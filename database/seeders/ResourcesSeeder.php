<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResourcesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resources')->insert([
            'id' => 1,
            'name' => 'minerals',
        ]);

        DB::table('resources')->insert([
            'id' => 2,
            'name' => 'gas'
        ]);
    }
}
