<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ShopController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth', 'update_resources', 'update_last_activity']], function() {
    Route::get('/home', [HomeController::class, 'index'])->name('home');;

    Route::prefix('shop')->group(function(){
        Route::get('/', [ShopController::class, 'index'])->name('shop');
        Route::get('/purchase/{id}', [ShopController::class, 'show'])->name('purchase');
        Route::post('/purchase', [ShopController::class, 'store'])->name('store');
    });

});

Route::put('/restart', [GameController::class, 'restart'])->middleware(['auth', 'update_last_activity'])->name('restart');
