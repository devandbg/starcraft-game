<?php

namespace App\Http\Controllers;

use App\Models\Mine;
use App\Services\ShopServices;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;

class ShopController extends Controller
{
    /**
     *  Show the shop with all products available
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $mines = Mine::all();

        return view('shop.index',compact('mines'));
    }

    /**
     *  Show info about the product with confirm button
     *
     * @param $id
     * @return Application|Factory|View
     */
    public function show($id)
    {
        $shop = new ShopServices($id);
        $shop->findMine();
        $mine = $shop->getMineParameters();

        return view('shop.purchase')->with(['mine' => $mine]);
    }

    /**
     *  Buy the item and redirect to the shop view (index function above) with flash message
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $id = $request->input('id');

        $shop = new ShopServices($id);
        $shop->findMine();

        $validate = $shop->validateMinePurchase();

        if($validate['status']) {
            $response = $shop->buy();
            return redirect()->route('shop')->with('success', $response['message']);
        } else {
            return redirect()->route('shop')->with('error', $validate['message']);
        }
    }
}
