<?php

namespace App\Http\Controllers;

use App\Services\GameService;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
    public function restart()
    {
        $user = Auth::user();
        $game = new GameService($user);
        $game->restart();

        return redirect()->route('home');
    }
}
