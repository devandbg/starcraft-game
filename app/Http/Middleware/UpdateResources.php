<?php

namespace App\Http\Middleware;

use App\Models\Mine;
use App\Models\Resource;
use App\Services\MineServices;
use App\Services\ResourcesServices;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class UpdateResources
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();

        $minesCounts = MineServices::getMinesCount();
        $mineralMineCount = $minesCounts['mineralMineCount'];
        $gasMineCount = $minesCounts['gasMineCount'];

        $lastActivity = $user->last_activity;
        $finishDate = Carbon::now();

        $diffInSeconds = $finishDate->diffInSeconds($lastActivity);
        $diffTimeInMinutes = floor($diffInSeconds / 60);
        $notUsedSeconds = $diffInSeconds % 60;

        $producedGas = ($gasMineCount * 5) * $diffTimeInMinutes;
        $producedMinerals = ($mineralMineCount * 5) * $diffTimeInMinutes;

        $gasId = Resource::select('id')->where('name', 'gas')->first()->id;
        $mineralsId = Resource::select('id')->where('name', 'minerals')->first()->id;

        $resourcesCounts = ResourcesServices::getResources();
        $gasCount = $resourcesCounts['gas'];
        $mineralsCount = $resourcesCounts['minerals'];

        $gasCount += $producedGas;
        $mineralsCount += $producedMinerals;

        $user->resources()->updateExistingPivot($gasId, ['count' => $gasCount]);
        $user->resources()->updateExistingPivot($mineralsId, ['count' => $mineralsCount]);

        $request->request->add(['notUsedSeconds' => $notUsedSeconds]);
        return $next($request);
    }
}
