<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdateLastActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $notUsedSeconds = $request['notUsedSeconds'];

        $now = Carbon::now();
        $now->subSeconds($notUsedSeconds);

        $user = Auth::user();

        $user->update([
            'last_activity' => $now,
        ]);

        return $next($request);
    }
}
