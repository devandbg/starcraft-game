<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    use HasFactory;

    protected $table='resources';
    protected $guarded='id';

    public function users()
    {
        $this->belongsToMany(User::class, 'resource_user', 'resource_id', 'user_id');
    }
}
