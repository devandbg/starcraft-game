<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mine extends Model
{
    use HasFactory;

    protected $table='mines';
    protected $guarded='id';

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
