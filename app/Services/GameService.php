<?php


namespace App\Services;


use App\Models\Mine;
use App\Models\Resource;
use App\Models\User;

class GameService
{
    /**
     * @var User $user
     */
    private $user;

    /**
     * GameService constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     *  Called when a user is registered and set the resources and mines for the user
     *  Used once In RegisterController
     *
     *  @return void
     */
    public function start()
    {
        $mineralsId = Resource::select('id')->where('name', 'minerals')->first();
        $gasId = Resource::select('id')->where('name', 'gas')->first();
        $mineralMineId = Mine::select('id')->where('name', 'Mineral Mine')->first();
        $gasMineId = Mine::select('id')->where('name', 'Gas Mine')->first();

        $this->user->resources()->attach($mineralsId, ['count' => 500]);
        $this->user->resources()->attach($gasId, ['count' => 0]);
        $this->user->mines()->attach($mineralMineId, ['count' => 1]);
        $this->user->mines()->attach($gasMineId, ['count' => 0]);
    }

    /**
     *  Start a new game and reset the resources and mines for the user
     *
     *  @return void
     */
    public function restart()
    {
        $mineralsId = Resource::select('id')->where('name', 'minerals')->first()->id;
        $gasId = Resource::select('id')->where('name', 'gas')->first()->id;

        $this->user->resources()->updateExistingPivot($mineralsId, ['count' => 500]);
        $this->user->resources()->updateExistingPivot($gasId, ['count' => 0]);

        $mineralMineId = Mine::select('id')->where('name', 'Mineral Mine')->first()->id;
        $gasMineId = Mine::select('id')->where('name', 'Gas Mine')->first()->id;

        $this->user->mines()->updateExistingPivot($mineralMineId, ['count' => 1]);
        $this->user->mines()->updateExistingPivot($gasMineId, ['count' => 0]);
    }
}
