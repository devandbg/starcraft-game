<?php


namespace App\Services;


use App\Models\Mine;
use App\Models\Resource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ShopServices
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $itemName;

    /**
     * @var float
     */
    private $itemPrice;

    /**
     * @var string
     */
    private $resourceName;


    /**
     * ShopServices constructor.
     * @param int $id
     */
    public function __construct(int $id = 0)
    {
        $this->id = $id;
    }

    /**
     *  Find mine model and set the fields
     */
    public function findMine()
    {
        $mine = Mine::find($this->id);
        $this->setMineParameters($mine);
    }

    /**
     * @param Mine $mine
     * @return mixed
     */
    private function setMineParameters(Mine $mine)
    {
        $this->itemName = $mine->name;
        $this->itemPrice = $mine->price;
        $this->resourceName = $this->setResourceName($this->itemName);
    }

    /**
     *  Return the resourceName
     *
     * @param string $mineName
     * @return string
     */
    private function setResourceName(string $mineName)
    {
        $resourceName = '';

        switch($mineName)
        {
            case 'Mineral Mine':
                $resourceName = 'Gas';
                break;
            case 'Gas Mine':
                $resourceName = 'Minerals';
                break;
        }

        return $resourceName;
    }

    /**
     *  Return mine parameters
     *
     * @return array
     */
    public function getMineParameters()
    {
        return ['id' => $this->id, 'name' => $this->itemName, 'price' => $this->itemPrice, 'resourceCost' => $this->resourceName];
    }

    /**
     *  Making validation for if you can buy or not
     *
     * @return string[]
     */
    public function validateMinePurchase()
    {
        if($this->itemName == 'Mineral Mine') {
            return [
                'status' => false,
                'message' => "You are not allowed to buy {$this->itemName}"
            ];
        }

        $resources = ResourcesServices::getResources();

        $minerals = $resources['minerals'];
        $gas = $resources['gas'];

        $status = false;

        switch ($this->resourceName)
        {
            case 'Minerals':
                if($this->itemPrice <= $minerals) {
                    $status = true;
                }
                break;
            case 'Gas':
                if($this->itemPrice <= $gas) {
                    $status = true;
                }
                break;
        }

        return [
            'status' => $status,
            'message' => "You dont have enough {$this->resourceName} to buy {$this->itemName} !"
        ];
    }

    /**
     *  Update the resources and mines count
     *  Return message
     *
     * @return string[]
     */
    public function buy()
    {
        $user = Auth::user();

        $resourceId = Resource::select('id')->where('name', $this->resourceName)->first()->id;
        $resourceCount = $user->resources()->where('resource_id',$resourceId)->first()->pivot->count;
        $resourceCount -= $this->itemPrice;
        $user->resources()->updateExistingPivot($resourceId, ['count' => $resourceCount]);

        $mineCount = $user->mines()->where('mine_id',$this->id)->first()->pivot->count;
        $mineCount++;
        $user->mines()->updateExistingPivot($this->id, ['count' => $mineCount]);

        return [
            'message' => "You successfully bought {$this->itemName} for {$this->itemPrice} {$this->resourceName}"
        ];
    }
}
