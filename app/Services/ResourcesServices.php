<?php


namespace App\Services;


use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ResourcesServices
{
    public static function getResources()
    {
        $resources =[];

        if(Auth::check()) {
            $user = User::find(Auth::id());

            foreach ($user->resources as $res) {
                $resources[$res->name] = $res->pivot->count;
            }
        }

        return $resources;
    }
}
