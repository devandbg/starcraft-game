<?php


namespace App\Services;


use App\Models\Mine;
use Illuminate\Support\Facades\Auth;

class MineServices
{
    public static function getMinesCount()
    {
        $mineralMineCount = 0;
        $gasMineCount = 0;

        if(Auth::check()) {
            $user = Auth::user();
            $mineralMineId = Mine::select('id')->where('name', 'Mineral Mine')->first()->id;
            $gasMineId = Mine::select('id')->where('name', 'Gas Mine')->first()->id;

            $mineralMineCount = $user->mines()->where('mine_id', $mineralMineId)->first()->pivot->count;
            $gasMineCount = $user->mines()->where('mine_id', $gasMineId)->first()->pivot->count;

        }
        return $minesCount = [
            'mineralMineCount' => $mineralMineCount,
            'gasMineCount' => $gasMineCount,
        ];
    }
}
