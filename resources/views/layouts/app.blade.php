<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Bootstrap cdns -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">Miner Game</a>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <a href="{{ route('shop') }}">Shop</a>
                    <!-- Left Side Of Navbar -->
{{--                    ...--}}
                    <!-- End of Left Side navbar -->

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">

                        @auth
                            <li class="mines mr-5">
                                <span class="mr-2"><img class="" src="{{ asset('Images/MineralMine.png') }}" title="Mineral Mine" alt="Mineral Mine" height="20px"> {{ @$minesCount['mineralMineCount'] }}</span>
                                <span class="mr-2"><img class="" src="{{ asset('Images/GasMine.png') }}" title="Gas Mine" alt="Gas Mine" height="30px"> {{ @$minesCount['gasMineCount'] }}</span>
                            </li>
                            <li class="resources mr-5">
                                <span class="text-success mr-2" title="Minerals">{{ @$resources['minerals'] }} <i class="fas fa-atom"></i></span>
                                <span class="text-danger" title="Gas">{{ @$resources['gas'] }}  <i class="fas fa-burn"></i></span>
                            </li>

                            <li class="name-logout">
                                <span class="mr-1">{{ Auth::user()->name }}</span>

                                <a class="ml-1" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        @endauth

                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
