@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row  mt-5">

        @if($minesCount['mineralMineCount'] > 0)
            <ul class="col-6 d-flex justify-content-center align-self-start flex-wrap list-unstyled">
                @for($i = 0; $i < $minesCount['mineralMineCount']; $i++)
                    <li>
                        <div class="mine-minerals mb-4 mr-4">
                            <img class="" src="{{ asset('Images/MineralMine.png') }}" title="Mineral Mine" alt="Mineral Mine" height="100px">
                            <span class="d-block text-center">Mine Minerals</span>
                        </div>
                    </li>
                @endfor
            </ul>
        @endif

        @if($minesCount['gasMineCount'] > 0)
            <ul class="col-6 d-flex justify-content-center flex-wrap list-unstyled">
                @for($i = 0; $i < $minesCount['gasMineCount']; $i++)
                    <li>
                        <div class="mine-gas mb-4 mr-4">
                            <img class="" src="{{ asset('Images/GasMine.png') }}" title="Gas Mine" alt="Gas Mine" height="100px">
                            <span class="d-block text-center">Mine Gas</span>
                        </div>
                    </li>
                @endfor
            </ul>
        @endif

    </div>
</div>
@endsection
