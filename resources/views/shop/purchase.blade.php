@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="confirm-card mt-5 p-3 w-50 mx-auto text-center bg-secondary text-white">
            <p class="">Do you confirm buying <span class="text-success">1 {{ $mine['name'] }}</span> for <span class="text-danger">{{ $mine['price'] }} {{ $mine['resourceCost'] }}</span></p>
            <a class="btn btn-success" onclick="event.preventDefault(); document.getElementById('buy-mine-form').submit();">Confirm</a>
            <form id="buy-mine-form" action="{{ route('store') }}" method="POST" class="d-none">
                @csrf
                @method('POST')
                <input type="text" name="id" value="{{ $mine['id'] }}">
            </form>
        </div>
    </div>
@endsection
