@extends('layouts.app')

@section('content')
    <div class="container ">

        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @elseif(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif


        <h3>Buy: </h3>
        <ul class="mt-3">
            @foreach($mines as $mine)
                <li>
                    <a href="{{ route('purchase', $mine->id) }}">{{ $mine->name }}</a>
                </li>
            @endforeach

            <li><a href="">...</a></li>
        </ul>
    </div>
@endsection
